Hello Friends,

If you want to configure HPA (Horizontal Pod Autoscaler), make sure you follow this step

1. Make sure the application deployment script use resources limit and request (you can check the sample script in the deployment-apids-digisign.yaml)

2. Install Metric Server, apply this command in your master node,

    `kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml`

3. edit the metric server deployment with `kubectl edit deployment [your deployment name] -n [namespace]`, add this line below args:,

    `- --kubelet-insecure-tls`

    `- --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname`

     and save the deployment

4. after that, apply this,

    `kubectl autoscale deployment [your deployment name] --cpu-percent=50 --min=1 --max=10 -n [namespace]`

references : 

[1] https://medium.com/mabar/today-i-learned-enabling-horizontal-pod-autoscaler-hpa-in-kubernetes-489126696990

[2] https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/

[3] https://kubernetes.io/blog/2016/07/autoscaling-in-kubernetes/

[4] https://github.com/kubernetes-sigs/metrics-server#configuration



